
import { searchBalance } from '../../../services/litecoin'

export default {

  async litecoinBalance(context, data) {
  // async litecoinBalance({ commit }, data) {
    try {
      // console.log('store.modules.litecoin.ACTIONS.context ::; ', context)
      console.log('store.modules.litecoin.ACTIONS.data ::; ', data)
      const response = await searchBalance(data)
      console.log('store.modules.litecoin.ACTIONS.response ::; ', response)
      console.log('store.modules.litecoin.ACTIONS.response.data ::; ', response.data)
      return response.data
      // commit('SET_LITECOIN', response.data)
    } catch (error) {
      console.log('store.modules.litecoin.ACTIONS.error ::; ', error)
      console.log('store.modules.litecoin.ACTIONS.error.response ::; ', error.response)
      throw error.response
    }
  }

  // async getInfoClient ({ commit }, data) {
  //   try {
  //     // console.log('fetchClient() ::; ', fetchClient())
  //     const response = await fetchClient(data)
  //     // console.log('response.getInfoClient ::; ', response)
  //     // console.log('response.data.getInfoClient ::; ', response.data)
  //     commit('SET_CLIENT', response.data)
  //   } catch (error) {
  //     throw error.response
  //     console.log('getInfoClient.error ::; ', error)
  //     // throw error.response.data
  //   }
  // }

}
