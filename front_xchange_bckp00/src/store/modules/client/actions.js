
import { addClient, fetchClient } from '../../../services/client'

export default {
  async createClient(context, data) {
    try {
      // console.log('context ::; ', context);
      // console.log('data ::; ', data)
      const response = await addClient(data)
    } catch (error) {
      throw error.response
    }
  },
  async getInfoClient ({ commit }, data) {
    try {
      // console.log('fetchClient() ::; ', fetchClient())
      const response = await fetchClient(data)
      // console.log('response.getInfoClient ::; ', response)
      // console.log('response.data.getInfoClient ::; ', response.data)
      commit('SET_CLIENT', response.data)
    } catch (error) {
      throw error.response
      console.log('getInfoClient.error ::; ', error);
      // throw error.response.data
    }
  }
}
