
import Vue from 'vue'
import Vuex from 'vuex'

// import environment from './modules/environment/environment'
// import environmentNeed from './modules/environment-need/environmentNeed'
// import equipment from './modules/equipment/equipment'
import client from './modules/client/client'

Vue.use(Vuex)

const modules = {
  // environment,
  // environmentNeed,
  // equipment,
  client
}

const store = new Vuex.Store({
  modules
})

export default store
