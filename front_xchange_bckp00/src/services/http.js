// axios.defaults.baseURL = 'http://localhost:3000/api/'

import axios from 'axios'

const http = axios.create({
  baseURL: 'http://localhost:3000/litecoin'
  // baseURL: 'http://localhost:3000/bitcoin'
  // baseURL: 'http://localhost:3000/nano'
})

export default http
