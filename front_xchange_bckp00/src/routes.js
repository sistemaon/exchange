import ClientPage from './components/client/client.vue'
// import EquipmentPage from './components/equipment/equipment.vue'
// import EnvironmentPage from './components/environment/environment.vue'
// import DashboardPage from './components/dashboard/dashboard.vue'

export const routes = [
  // { path: '/', component: WelcomePage },
  // { path: '/dashboard', component: DashboardPage },
  { path: '/client', component: ClientPage }
  // { path: '/environment', component: EnvironmentPage },
  // { path: '/equipment', component: EquipmentPage }
  // { path: '/', component: MenuPage }
]
