
// HASH TEST

// 419818719914481
///////////////////

const express = require('express');
const router = express.Router();

const raiBlocks = require('raiblocks')

// configure connection options
const optionsNano = {
  host: '::1',
  port: 55000
}

const nano = new raiBlocks(optionsNano)

// account create
router.post('/account', async (req, res, next) => {
  // const coin = (req.body.coin || 'nano').toUpperCase()
  const WALLET = '240612B03A6AAC34AF3032EFCA13BD10990586DE23266A2869000CAFB5BF74B6'
  const wallet = req.body.wallet || WALLET
  try {
    // const result = await actions.accountCreate(COINS[coin])(WALLET)

    const result = await nano.accountCreate(wallet)
    console.log('accountCreate ::; ', result)
    res.json({'account': result, 'wallet': wallet})
  } catch (error) {
    console.log(error)
  }
})

// wallet create
router.post('/wallet', async (req, res, next) => {
  // const coin = (req.body.coin || 'nano').toUpperCase()
  // const wallet = req.body.wallet || WALLET
  try {
    // const result = await actions.accountCreate(COINS[coin])(WALLET)
    const result = await nano.walletCreate()
    console.log('walletCreate ::; ', result)
    res.json(result)
  } catch (error) {
    console.log(error)
  }
})

module.exports = router;
