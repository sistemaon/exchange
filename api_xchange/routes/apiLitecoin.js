
// HASH TEST

// 48948919141
///////////////////

const express = require('express');
const router = express.Router();

const litecoin = require('litecoin');

// configure connection options
const optionsLitecoin = {
  host: '127.0.0.1',
  port: 19332,
  // port: 19335,
  user: 'bitcoin',
  pass: 'local321',
  timeout: 30000,
  ssl: false/* true,
  sslStrict: true,
  sslCa: fs.readFileSync(__dirname + '/myca.cert')*/
};

const ltc = new litecoin.Client(optionsLitecoin)

const getBalance = (req, res) => {
  // const account = req.body.account || '*'
  const account = req.body.account
  const minConf = 1

  ltc.getBalance(account, minConf, (err, balance) => {
    if (err) console.error(err)
    console.log('Balance ::; ', balance)
    res.json({'Account: ': account, 'Balance': balance})
  })

}

// Returns a new bitcoin address for receiving payments.
//  If [account] is specified payments received with the address will be credited to [account].
const getNewAddress = (req, res) => {
  const account = req.body.account || ''
  ltc.getNewAddress(account, (err, data) => {
    if (err) console.error(err)
    console.log('Address ::; ', data)
    res.json({'Address': data, 'Account': account})
    // {
    // "Address": "muRv7Csx4Knv8MjwnqrJmeJRJXuiy1iYEi",
    // "Account": ""
    // }
  })
}

// Returns the account associated with the given address.
const getAccount = (req, res) => {
  const address = req.body.address || '';
  // if(address != String) res.json({'Warning': 'address not specified, does not exist or invalid Litecoin address'});

  ltc.getAccount(address, (err, data) => {
    if (err) console.error(err);
    console.log('Account ::; ', data);
    res.json({'Account ': data, 'Address ': address})
  })
}

// Returns the current bitcoin address for receiving payments to this account.
// If <account> does not exist, it will be created along with an associated new address that will be returned.
const getAccountAddress = (req, res) => {
  const account = req.body.account || '';
  // if(address != String) res.json({'Warning': 'address not specified, does not exist or invalid Litecoin address'});
  ltc.getAccountAddress(account, (err, data) => {
    if (err) console.error(err);
    console.log('Address ::; ', data);
    res.json({'Address ': data, 'Account ': account})
  })
}

const validateAddress = (req, res) => {
  const address = req.body.address || '';
  // if(address != String) res.json({'Warning': 'address not specified, does not exist or invalid Litecoin address'});

  ltc.validateAddress(address, (err, data) => {
    if (err) console.error(err);
    console.log('Info ::; ', data);
    res.json({'Info ': data})
  })

}

const listReceivedByAddress = (req, res) => {
  // const address = req.body.address || '';
  // if(address != String) res.json({'Warning': 'address not specified, does not exist or invalid Litecoin address'});

  ltc.listReceivedByAddress((err, data) => {
    if (err) console.error(err);
    console.log('List ::; ', data);
    res.json({'List ': data})
  })

}

const listUnspent = (req, res) => {
  // const address = req.body.address || '';
  // if(address != String) res.json({'Warning': 'address not specified, does not exist or invalid Litecoin address'});

  ltc.listUnspent((err, data) => {
    if (err) console.error(err);
    console.log('Unspent ::; ', data);
    res.json({'Unspent ': data})
  })

}

const listLockUnspent = (req, res) => {

  ltc.listLockUnspent((err, data) => {
    if (err) console.error(err);
    console.log('LockUnspent ::; ', data)
    res.json({'LockUnspent ': data})
  })

}

const sendFrom = (req, res) => {

  // const address = req.body.address || '';
  // if(address != String) res.json({'Warning': 'address not specified, does not exist or invalid Litecoin address'});
  const fromAccount = req.body.fromAccount;
  const toAddress = req.body.toAddress;
  const amount = req.body.amount;

  ltc.sendFrom(fromAccount, toAddress, amount, (err, data) => {
    if (err) console.error(err);
    console.log('TransactionId ::; ', data);
    res.json({'TransactionId ': data})
  })
 // "TransactionId ": "9d1c4c4e17e01d48eb27a1206ae5d3de1db6e7eeafae0d68708ce3994d912953"
 // "fromAccount": "muRv7Csx4Knv8MjwnqrJmeJRJXuiy1iYEi",
 // "toAddress": "mnsrDT2mo312yk4S2a8wQQ47y8AsFncWY3" -> its account: mvbW1Y896Wu8qtTdo8vbia5pCaVyU2nxFo
}

// //txtid: ID da transaçao pego no listunsped
// //receipient: Endereço do destinatario do valor
// //amount: Valor a ser enviado
// //sender: Endereço do remetente quem esta enviando
// //change: Troco para carteira de envio: (saldo da origem - valor enviado) - taxa da transaçao (0.001)
const createRawTransaction = (req, res) => {
  const txid = req.body.txid
  const recipient = req.body.recipient
  const amount = req.body.amount
  const sender = req.body.sender
  const change = req.body.change
  ltc.createRawTransaction([{
      txid: txid,
      vout: 0
  }], {[recipient]: amount, [sender]: change}, (err, data) => {
      if (err) {
        return console.log('err ::; ', err);
      }
      console.log('hex ::; ', data);
      return data;
  })
    // txid ::; c9a3df4b56da716973f523ffedf260ae42b4ff66e63607dfcc34318fca891db8
    // recipient ::; mzq1ZW4en7LUWzXf271XReN1jKTuK9EjpT
    // amount ::; 3
    // sender ::; n4ciqjJpeo3o47P3iAzQXdyvoE4ot8rQuW
    // change ::; 0.1
    /*
      {
      	"txid": "",
      	"recipient": "",
      	"amount": 3,
      	"sender": "",
      	"change": 0.1
      }
    */
// 0100000001b81d89ca8f3134ccdf0736e666ffb442ae60f2edff23f5736971da564bdfa3c90000000000ffffffff0200a3e111000000001976a914d3d49a93446ddb874936586d5ed7da57565015e188ac80969800000000001976a914fd62287817bd0ba1d7f0c698593d483ba1f8c7af88ac00000000
}

const getRawTransaction = (req, res) => {
  const txid = req.body.txid
  ltc.getRawTransaction(txid, (err, data) => {
    if (err) console.error('Error ::; ', err);
    console.log('rawTransaction ::; ', data);
    res.json({'rawTransaction ': data})
  })
}

const sendRawTransaction = (req, res) => {
  const hex = req.body.hex
  ltc.sendRawTransaction(hex, (err, data) => {
    if (err) console.error('Error ::; ', err);
    console.log('rawTransaction ::; ', data);
    res.json({'rawTransaction ': data})
  })
}

const getTxOut = (req, res) => {
  const id = req.body.id
  ltc.getTxOut(id, 1, (err, data) => {
    if (err) console.error(err);
    console.log('utxo ::; ', data);
    res.json({'utxo ': data})
  })
}

const sendTransaction = (req, res) => {
  /*
      {
        "Address ": "mgv1zSREVkpq2XSVZFq6icFgcB9uUv7rEu",
        "Account ": "mzq1ZW4en7LUWzXf271XReN1jKTuK9EjpT"
      },
      {
        "Address ": "mnFB7th9PeWCpf5DBa19uLLrnYhtZKfgsk",
        "Account ": "n4ciqjJpeo3o47P3iAzQXdyvoE4ot8rQuW"
      },
      {
        "Address": "mvzd1XazAyqbtnMLrrDr5FFgCKTS7nnGVQ",
        "Account": "erni"
      },
      {
        "Address": "mgDmH7abBo7fwAPgezEmyduMGScMBcK86f",
        "Account": "souza"
      }
  */
  const fromAccount = req.body.fromAccount || 'erni'; // n4ciqjJpeo3o47P3iAzQXdyvoE4ot8rQuW -> BALANCE 8
  const toAddress = req.body.toAddress || 'mgDmH7abBo7fwAPgezEmyduMGScMBcK86f'; // mgv1zSREVkpq2XSVZFq6icFgcB9uUv7rEu
  const amount = req.body.amount || 1.23109; // 3
  // const txid = req.body.txid
  // const recipient = req.body.recipient
  // const amount = req.body.amount
  const sender = req.body.sender || 'mvzd1XazAyqbtnMLrrDr5FFgCKTS7nnGVQ'
  const change = req.body.change || 0.001

  ltc.sendFrom(fromAccount, toAddress, amount, (err, transactionId) => {
    if (err) console.error(err);
    console.log('sendFrom -> TransactionId ::; ', transactionId);
    // res.json({'TransactionId ': transactionId})
    const txid = transactionId
    ltc.createRawTransaction([{
        txid: txid,
        vout: 0
    }], {[toAddress]: amount, [sender]: change}, (err, hexId) => {
        if (err) console.log('err ::; ', err);
        console.log('createRawTransaction -> HexId ::; ', hexId);

        const hexString = hexId
        ltc.decodeRawTransaction(hexString, (err, dataDecoded) => {
          if (err) console.log('err ::; ', err);
          console.log('decodeRawTransaction -> dataDecoded ::; ', dataDecoded);
          // console.log('decodeRawTransaction -> dataDecoded.txid ::; ', dataDecoded.txid);

          // const txidDecoded = dataDecoded.txid
          ltc.getRawTransaction(txid, (err, representation) => {
            if (err) console.log('err ::; ', err);
            console.log('getRawTransaction -> representation ::; ', representation);

            const hexStringSign = representation
            ltc.signRawTransaction(hexStringSign, (err, resultRawTransaction) => {
              if (err) console.log('err ::; ', err);
              console.log('signRawTransaction -> resultRawTransaction ::; ', resultRawTransaction);

              const hexStringSend = resultRawTransaction.hex
              // console.log('hexStringSend.HEX ::; ', hexStringSend);
              ltc.sendRawTransaction(hexStringSend, (err, sendRawTransactionId) => {
                if (err) console.log('err ::; ', err);
                console.log('sendRawTransaction -> sendRawTransactionId ::; ', sendRawTransactionId);
              })
            })
          })
        })
    })
  })
}


////////////////////////////// FRONT FRONT FRONT

// Returns a new bitcoin address for receiving payments.
//  If [account] is specified payments received with the address will be credited to [account].
const getNewAddressFront = (req, res) => {
  const account = req.body.account || ''
  ltc.getNewAddress(account, (err, data) => {
    if (err) console.error(err);
    console.log('Address ::; ', data);
    const info = {'Address': data, 'Account': account}
    res.json(info)
    // {
    // "Address": "muRv7Csx4Knv8MjwnqrJmeJRJXuiy1iYEi",
    // "Account": ""
    // }
  })
}

// Returns the current bitcoin address for receiving payments to this account.
// If <account> does not exist, it will be created along with an associated new address that will be returned.
const getAccountAddressFront = (req, res) => {
  const account = req.body.account || '';
  // if(address != String) res.json({'Warning': 'address not specified, does not exist or invalid Litecoin address'});
  ltc.getAccountAddress(account, (err, address) => {
    if (err) console.error(err);
    console.log('Address ::; ', address);
    const info = {'Address': address, 'Account': account}
    res.json(info)
  })
}

/////////////////////////// FRONT
//////////////////// FRONT

router.post('/front-address', getNewAddressFront)
router.post('/front-address-account', getAccountAddressFront)
router.post('/front-balance', getBalance)

/////////////////// FRONT

//ok
router.post('/balance', getBalance);
//ok first step
router.post('/address', getNewAddress);
//ok
router.post('/account', getAccount);
//ok
router.post('/account-address', getAccountAddress);
//ok
router.post('/validate-address', validateAddress);
//ok
router.post('/list-received-address', listReceivedByAddress);
//ok
router.post('/list-unspent', listUnspent);
// in work progress, transaction // TODO: transaction (txid, utxo)
router.post('/send', sendFrom);
//ok
router.post('/list-lock-unspent', listLockUnspent);
//ok
router.post('/create-transaction', createRawTransaction);
//ok
router.post('/txout', getTxOut);
//ok
router.post('/send-transaction', sendRawTransaction);
//ok
router.post('/transaction', getRawTransaction);
//ok
router.post('/transaction-test', sendTransaction);

module.exports = router;


/*

  https://en.bitcoin.it/wiki/Original_Bitcoin_client/API_calls_list
  https://en.bitcoin.it/wiki/Raw_Transactions
  https://github.com/litecoin-project/litecoind-rpc
  https://github.com/litecoin-project/litecore-lib
  https://github.com/litecoin-project/litecore-lib/blob/master/docs/examples.md#create-a-transaction
  http://testnet.litecointools.com/

**/





//////////////////////// CONFIG API SIGN SEND RAW TRANSACTION UNSPENT UTXO
//
// const litecoin = require('litecoin');
//
// const client = new litecoin.Client({
//   "host": "localhost.com",
//   "port": 9332,
//   "user": "litecoin",
//   "pass": "y3a5f17cat@pass",
//   "timeout": 30000,
//   "ssl": false
// });
//
// function getBalance(address) {
//     client.getBalance(address, function (err, ret) {
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('getBalance', ret)
//     })
// }
//
// function sendFrom(from, to, amount) {
//     client.sendFrom(from, to, amount, (err, txid) => {
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('sendFrom', txid);
//     });
// }
//
// function listUnspent() {
//     client.listUnspent((err, ret) => {
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('listUnspent', ret);
//     });
// }
//
// //txtid: ID da transaçao pego no listunsped
// //receipet: Endereço do destinatario do valor
// //amount: Valor a ser enviado
// //sender: Endereço do remetente quem esta enviando
// //change: Troco para carteira de envio: (saldo da origem - valor enviado) - taxa da transaçao (0.001)
// function createRawTransaction (txid, recipient, amount, sender, change) {
//     client.createRawTransaction([{
//         txid: txid,
//         vout: 0,
//     }], {[recipient]: amount, [sender]: change}, (err, ret) => {
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('createRawTransaction', ret);
//         return ret;
//     });
// }
//
// function getTxOut(id) {
//     client.getTxOut(id, 1, (err, ret) => {
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('getTxOut', ret);
//
//     });
// }
//
// function getTransaction(id) {
//
//     client.getTransaction(id, (err, ret) => {
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('getTransaction', ret);
//
//     });
// }
//
// function getRawTransaction(id) {
//     client.getRawTransaction(id, (err, ret) => {
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('getRawTransaction', ret);
//
//     });
// }
//
// function signRawTransaction(hex) {
//     client.signRawTransaction(hex, (err, ret) => {
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('signRawTransaction', ret);
//         return ret;
//     });
// }
//
// function sendRawTransaction(hex) {
//     client.sendRawTransaction(hex, (err, ret) => {
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('sendRawTransaction', ret);
//         return ret;
//     });
// }
//
// function setAccount(account, address) {
//     client.setAccount(account, address, (err, account)=>{
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('setAccount', account);
//     });
// }
//
// function listAccounts() {
//     client.listAccounts((err, accounts)=>{
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('listAccounts', accounts);
//     });
// }
//
// function listAddressGroupings() {
//     client.listAddressGroupings((err, accounts)=>{
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('listAddressGroupings', accounts);
//     });
// }
//
// function listLockUnspent() {
//     client.listLockUnspent((err, accounts)=>{
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('listLockUnspent', accounts);
//     });
// }
//
// function validateAddress(address) {
//     client.validateAddress(address, (err, address)=>{
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('validateAddress', address);
//     });
// }
//
// function listTransactions(account) {
//     client.listTransactions(account, (err, ret)=>{
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('listTransactions', ret);
//     });
// }
//
// function getAddressesByAccount(account) {
//     client.getAccountAddress(account, (err, ret)=>{
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('getAddressesByAccount', ret);
//     });
// }
//
// function getRawMempool() {
//     client.getRawMempool((err, ret)=>{
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('getRawMempool', ret);
//     });
// }
//
//
// function getMiningInfo() {
//     client.getMiningInfo((err, ret)=>{
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('verifyChain', ret);
//     });
// }
//
// function estimateFee(blocks) {
//     client.estimateFee(blocks, (err, ret)=>{
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('estimateFee', ret);
//     });
// }
//
// function dumpPrivKey(address) {
//     client.dumpPrivKey(address, (err, ret)=>{
//         if (err) {
//             return console.log('err', err);
//         }
//
//         console.log('dumpPrivKey', ret);
//     });
// }
