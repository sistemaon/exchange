
// HASH TEST

// 14111963
///////////////////

const express = require('express')
const router = express.Router()


const bitcoincore = require('bitcoin-core')
// const options = {
//   host: '198.50.128.9',
//   network: 'testnet',
//   password: 'local321',
//   port: 8332,
//   username: 'bitcoin'
// }
const options = {
  host: '127.0.0.1',
  network: 'testnet',
  password: 'local321',
  port: 18342,
  username: 'bitcoin'
}
const btcClient = new bitcoincore(options)

// const validateAddress = async () => {
//   try {
//     const address = 'mtpncZxJUN3m5pQfM1gpkbTiGPCJTb1YBd'
//     const account = await btcClient.validateAddress(address);
//     console.log('account ::; ', account);
//   } catch (e) {
//     console.log('e ::; ', e);
//   }
// }
// validateAddress()

// const setAccount = async () => {
//   try {
//     const address = 'mtpncZxJUN3m5pQfM1gpkbTiGPCJTb1YBd'
//     const account = await btcClient.setAccount(address);
//     console.log('account ::; ', account);
//     // account ::;  null // account ::: mtpncZxJUN3m5pQfM1gpkbTiGPCJTb1YBd
//   } catch (e) {
//     console.log('e ::; ', e);
//   }
// }
// setAccount()

const getNewAddress = async (req, res) => {
  try {
    const account = req.body.account || ''
    const newAddress = await btcClient.getNewAddress(account)
    console.log('newAddress ::; ', newAddress)
    res.json({account: account, address: newAddress})
    // {
    //   "account": "Souza",
    //   "address": "n4MddQoqE64rrUSCsKz2Qf4UHc15r1TNLQ"
    // }
    // {
    //   "account": "ExchangeAccount",
    //   "address": "mricJeCPrTq33EAvw5ghm4QRjhyhYmwrr2"
    // }
  } catch (e) {
    console.log('error.getNewAddress ::; ', e)
  }
}
// getNewAddress()

const getBalance = async (req, res) => {
  try {
    const account = req.body.account || '*'
    // const account = '*'
    const minConf = 0
    const balance = await btcClient.getBalance(account, minConf)
    console.log('balance ::; ', balance)
    // res.json({account: account, balance: balance})
    res.json({account: account, balance: balance})
  } catch (e) {
    console.log('e.BTC ::; ', e)
  }
}
// getBalance()

// Returns the account associated with the given address.
const getAccount = async (req, res) => {
  try {
    const address = req.body.address || ''
    const account = await btcClient.getAccount(address)
    console.log('account ::; ', account)
    res.json({address: address, account: account})
  } catch (e) {
    console.log('e.BTC ::; ', e)
  }
}
// getAccount()

// Returns the current bitcoin address for receiving payments to this account.
// If <account> does not exist, it will be created along with an associated new address that will be returned.
const getAccountAddress = async (req, res) => {
  try {
    const account = req.body.account || ''
    const address = await btcClient.getAccountAddress(account)
    console.log('address ::; ', address)
    res.json({account: account, address: address})
  } catch (e) {
    console.log('e.BTC ::; ', e)
  }
}
// getAccountAddress()

// <amount> is a real and is rounded to 8 decimal places.
// Will send the given amount to the given address,
// ensuring the account has a valid balance using [minconf] confirmations.
// Returns the transaction ID if successful (not in JSON object).
const sendFrom = async (req, res) => {
  try {
    const fromAccount = req.body.fromAccount || ''
    const tobitcoinaddress = req.body.tobitcoinaddress || ''
    const amount = req.body.tobitcoinaddress || 0.01
    const txId = await btcClient.sendFrom(fromAccount, tobitcoinaddress, amount)
    console.log('txId ::; ', txId)
    res.json({txId: txId})
  } catch (e) {
    console.log('e.BTC ::; ', e)
  }
}
// sendFrom()

//ok first step
router.post('/address', getNewAddress)
router.post('/balance', getBalance)
router.post('/account', getAccount)
router.post('/accountaddr', getAccountAddress)

module.exports = router
