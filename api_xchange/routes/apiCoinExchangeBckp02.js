
// HASH TEST

// 1888948
///////////////////

const express = require('express');
const router = express.Router();
const axios = require('axios');

const baseURL = 'https://api.coinmarketcap.com'
const getBtcInfo = (req, res) => {
  axios.get(baseURL + '/v2/ticker/1/?convert=BRL')
  .then( (response) => {
    console.log('response.btc.info ::; ', response);
    console.log('response.data.btc.info ::; ', response.data);
    res.json(response.data)
  })
  .catch( (error) => {
    console.log('error.btc.info ::; ', error);
    res.json(error)
  })
}

const getLtcInfo = (req, res) => {
  axios.get(baseURL + '/v2/ticker/2/?convert=BRL')
  .then( (response) => {
    console.log('response.ltc.info ::; ', response);
    console.log('response.data.ltc.info ::; ', response.data);
    res.json(response.data)
  })
  .catch( (error) => {
    console.log('error.ltc.info ::; ', error);
    res.json(error)
  })
}

// const getBtcInfoAll = (req, res) => {
//   return axios.get(baseURL + '/v2/ticker/1/?convert=BRL')
//   .then( (response) => {
//     // console.log('response.btc.getBtcInfoAll ::; ', response);
//     // console.log('response.data.btc.getBtcInfoAll ::; ', response.data);
//     const btcData = response.data
//     // console.log('btcData ::; ', btcData);
//     return btcData
//     // res.json(btcData)
//   })
//   .catch( (error) => {
//     console.log('error.btc.info ::; ', error);
//     // res.json(error)
//   })
// }
// const getLtcInfoAll = (req, res) => {
//   return axios.get(baseURL + '/v2/ticker/2/?convert=BRL')
//   .then( (response) => {
//     // console.log('response.btc.getBtcInfoAll ::; ', response);
//     // console.log('response.data.ltc.getLtcInfoAll ::; ', response.data);
//     const ltcData = response.data
//     // console.log('ltcData ::; ', ltcData);
//     return ltcData
//     // res.json(ltcData)
//   })
//   .catch( (error) => {
//     console.log('error.ltc.info ::; ', error);
//     // res.json(error)
//   })
// }
// const BtcLtc = (req, res) => {
//   axios.all([getBtcInfoAll(), getLtcInfoAll()])
//   // .then((response) => {
//   .then(([btc, ltc]) => {
//     console.log('btc ::; ', btc);
//     console.log('ltc ::; ', ltc);
//     // console.log('getLtcInfoAll ::; ', getLtcInfoAll().ltcData);
//     // console.log('getBtcInfoAll ::; ', getBtcInfoAll().btcData);
//     // console.log('getLtcInfoAll ::; ', getLtcInfoAll);
//
//     // console.log('getBtcInfoAll ::; ', getBtcInfoAll);
//
//     // Both requests are now complete
//     // console.log('BtcLtc.acct ::; ', acct);
//     // console.log('BtcLtc.perms ::; ', perms);
//     // res.json(perms)
//     // res.json(response)
//     // console.log('ALL.BtcLtc.response ::; ', response);
//   })
//   .catch( (error) => {
//     console.log('error.BtcLtc ::; ', error);
//     // res.json(error)
//   })
//     // .then(axios.spread((acct, perms) => {
//     //   // Both requests are now complete
//     //   console.log('BtcLtc.acct ::; ', acct);
//     //   console.log('.BtcLtc.perms ::; ', perms);
//     // }));
// }

const getBtcInfoAsync = async ( ) => {
  try {
    const response = await axios.get(baseURL + '/v2/ticker/1/?convert=BRL')
    const btcData = response.data
    // console.log('btcData ::; ', btcData);
    return btcData
  } catch (error) {
    console.log('error.btc.info.getBtcInfoAsync ::; ', error);
  }
}

const getLtcInfoAsync = async ( ) => {
  try {
    const response = await axios.get(baseURL + '/v2/ticker/2/?convert=BRL')
    const ltcData = response.data
    // console.log('ltcData ::; ', ltcData);
    return ltcData
  } catch (error) {
    console.log('error.btc.info.getBtcInfoAsync ::; ', error);
  }
}
const BtcLtc = (req, res) => {
  axios.all([getBtcInfoAsync(), getLtcInfoAsync()])
  // .then((response) => {
  .then(([btc, ltc]) => {
    console.log('btc ::; ', btc);
    console.log('ltc ::; ', ltc);
    // console.log('getLtcInfoAll ::; ', getLtcInfoAll().ltcData);
    // console.log('getBtcInfoAll ::; ', getBtcInfoAll().btcData);
    // console.log('getLtcInfoAll ::; ', getLtcInfoAll);

    // console.log('getBtcInfoAll ::; ', getBtcInfoAll);
  })
  .catch( (error) => {
    console.log('error.BtcLtc ::; ', error);
    // res.json(error)
  })
}

router.get('/btc', getBtcInfo)
router.get('/ltc', getLtcInfo)
router.get('/btcltc', BtcLtc)

module.exports = router;

// const cryptoValue = (req, res) => {
//   const data = url
//   console.log(data);
//   console.log('res ::; ', res.json());
//   // return data
// }

// const searchAllCoin = () =>
//   fetch(`https://api.coinmarketcap.com/v1/ticker/`)
//     .then( data => { console.log('dataall ::; ', data); return data } );
//
//
// const searchSingleCoin = ( coin ) =>
//   fetch(`https://api.coinmarketcap.com/v1/ticker/${coin}/`)
//     .then( data => { console.log('data ::; ', data); return data } );
//     // fetch(`https://api.coinmarketcap.com/v1/ticker/bitcoin/`)
//     //   .then( data => { data.json() });
//     // const singleCoin = searchSingleCoin('bitcoin');
//
// export { searchAllCoin, searchSingleCoin }
