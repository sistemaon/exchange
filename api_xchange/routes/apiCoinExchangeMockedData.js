
// HASH TEST

// 1888948
///////////////////

const express = require('express');
const router = express.Router();
const axios = require('axios');

const baseURL = 'https://api.coinmarketcap.com'
const getBtcInfo = async (req, res) => {
  try {
    const response = await axios.get(baseURL + '/v2/ticker/1/?convert=BRL')
    console.log('response.btc.info ::; ', response);
    console.log('response.data.btc.info ::; ', response.data);
    res.json(response.data)
  } catch (error) {
    console.log('error.btc.info ::; ', error);
    res.json(error)
  }
}
const getLtcInfo = async (req, res) => {
  try {
    const response = await axios.get(baseURL + '/v2/ticker/2/?convert=BRL')
    console.log('response.ltc.info ::; ', response);
    console.log('response.data.ltc.info ::; ', response.data);
    res.json(response.data)
  } catch (error) {
    console.log('error.ltc.info ::; ', error);
    res.json(error)
  }
}



const getBtcInfoAsync = async ( ) => {
  try {
    const response = await axios.get(baseURL + '/v2/ticker/1/?convert=BRL')
    const btcData = response.data
    // console.log('btcData ::; ', btcData);
    return btcData
  } catch (error) {
    console.log('error.btc.info.getBtcInfoAsync ::; ', error);
  }
}
const getLtcInfoAsync = async ( ) => {
  try {
    const response = await axios.get(baseURL + '/v2/ticker/2/?convert=BRL')
    const ltcData = response.data
    // console.log('ltcData ::; ', ltcData);
    return ltcData
  } catch (error) {
    console.log('error.btc.info.getBtcInfoAsync ::; ', error);
  }
}
const BtcLtc = async (req, res) => {
  try {
    const [btc, ltc] = await axios.all([getBtcInfoAsync(), getLtcInfoAsync()])

    const btcPrice = btc.data.quotes.BRL.price
    const ltcPrice = ltc.data.quotes.BRL.price

    // const ltcAmountCoin = 2;
    // const ltcAmountMoney = ltcAmountCoin * ltcPrice
    // const ltcToBtcAmountCoin = ltcAmountMoney / btcPrice
    // console.log('ltcAmountMoney ::; ', ltcAmountMoney);
    // console.log('ltcToBtcAmountCoin ::; ', ltcToBtcAmountCoin);
    const ltcWallet = {
      address: 'sa1Fx=tRe0o19L1Csison',
      balance: 20.1337016
    }
    const ltcAmountMoneyFromWallet = ltcWallet.balance * ltcPrice
    const ltcToBtcAmountCoinFromWallet = ltcAmountMoneyFromWallet / btcPrice
    console.log('ltcAmountMoneyFromWallet ::; ', ltcAmountMoneyFromWallet)
    console.log('ltcToBtcAmountCoinFromWallet ::; ', ltcToBtcAmountCoinFromWallet)
    ////////////////////////////////////////////////////////
    ////////////////////// money = BRL, coin = cryptocurrency
    const btcAmountCoinBuy = 0.1 // 0.000009
    const btcAmountMoneyBuyToLtc = btcAmountCoinBuy * btcPrice
    const btcAmountCoinBuyToLtc = btcAmountMoneyBuyToLtc / ltcPrice
    console.log('////////////////////////////////////////////////////////')
    console.log('btcAmountCoinBuy ::; ', btcAmountCoinBuy)
    console.log('btcAmountMoneyBuyToLtc ::; ', btcAmountMoneyBuyToLtc)
    console.log('btcAmountCoinBuyToLtc ::; ', btcAmountCoinBuyToLtc)
    console.log('////////////////////////////////////////////////////////')
    const btcWalletReceiveCoin = {
      address: 'receiveCoin',
      balance: 0
    }
    if (ltcWallet.balance >= btcAmountCoinBuyToLtc) {
      // console.log('true');
      // console.log('ltcWallet.balance ::; ', ltcWallet.balance);
      // console.log('btcAmountCoinBuyToLtc ::; ', btcAmountCoinBuyToLtc);
      // true
      // ltcWallet.balance ::;  20.1337016
      // btcAmountCoinBuyToLtc ::;  9.490913985084939
      const changeLtcToBtc = ltcWallet.balance - btcAmountCoinBuyToLtc
      ltcWallet.balance = changeLtcToBtc
      console.log('changeLtcToBtc ::; ', changeLtcToBtc)
      console.log('ltcWallet.after.buy ::; ', ltcWallet)

      const btcReceived = btcWalletReceiveCoin.balance + btcAmountCoinBuy
      btcWalletReceiveCoin.balance = btcReceived
      console.log('btcReceived ::; ', btcReceived)
      console.log('btcWalletReceived.after.buy ::; ', btcWalletReceiveCoin)
    } else {
      console.log('false')
      // console.log('ltcWallet.balance ::; ', ltcWallet.balance);
      // console.log('btcAmountCoinBuyToLtc ::; ', btcAmountCoinBuyToLtc);
    }
    // const btcAmountCoin = 2;
    // const btcAmountMoney = btcAmountCoin * btcPrice
    // const btcToLtcAmountCoin = btcAmountMoney / ltcPrice
    // console.log('btcAmountMoney ::; ', btcAmountMoney);
    // console.log('btcToLtcAmountCoin ::; ', btcToLtcAmountCoin);
    const btcWallet = {
      address: 'asHke1tcHUnm81Tseason',
      balance: 50.0573633
    }
    const btcAmountMoneyFromWallet = btcWallet.balance * btcPrice
    const btcToLtcAmountCoinFromWallet = btcAmountMoneyFromWallet / ltcPrice
    console.log('btcAmountMoneyFromWallet ::; ', btcAmountMoneyFromWallet)
    console.log('btcToLtcAmountCoinFromWallet ::; ', btcToLtcAmountCoinFromWallet)

    console.log('btcPrice ::; ', btcPrice)
    console.log('ltcPrice ::; ', ltcPrice)
    res.json({btc: btc, ltc: ltc})
  } catch (error) {
    console.log('error.BtcLtc ::; ', error)
    res.json(error)
  }
}

router.get('/btc', getBtcInfo)
router.get('/ltc', getLtcInfo)
router.get('/btcltc', BtcLtc)

module.exports = router
