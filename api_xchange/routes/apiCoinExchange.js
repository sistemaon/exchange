
// HASH TEST

// 1888948
///////////////////

const express = require('express');
const router = express.Router();
const axios = require('axios');

const litecoin = require('litecoin');
const bitcoincore = require('bitcoin-core');

const optionsBitcoin = {
  host: '127.0.0.1',
  network: 'testnet',
  password: 'local321',
  port: 18342,
  username: 'bitcoin'
}

// configure connection options
const optionsLitecoin = {
  host: '127.0.0.1',
  port: 19332,
  // port: 19335,
  user: 'bitcoin',
  pass: 'local321',
  timeout: 30000,
  ssl: false/* true,
  sslStrict: true,
  sslCa: fs.readFileSync(__dirname + '/myca.cert')*/
}

const ltcClient = new litecoin.Client(optionsLitecoin)
const btcClient = new bitcoincore(optionsBitcoin)

const baseURL = 'https://api.coinmarketcap.com'
const getBtcInfo = async (req, res) => {
  try {
    const response = await axios.get(baseURL + '/v2/ticker/1/?convert=BRL')
    console.log('response.btc.info ::; ', response);
    console.log('response.data.btc.info ::; ', response.data);
    res.json(response.data)
  } catch (error) {
    console.log('error.btc.info ::; ', error);
    res.json(error)
  }
}
const getLtcInfo = async (req, res) => {
  try {
    const response = await axios.get(baseURL + '/v2/ticker/2/?convert=BRL')
    console.log('response.ltc.info ::; ', response);
    console.log('response.data.ltc.info ::; ', response.data);
    res.json(response.data)
  } catch (error) {
    console.log('error.ltc.info ::; ', error);
    res.json(error)
  }
}



const getBtcInfoAsync = async ( ) => {
  try {
    const response = await axios.get(baseURL + '/v2/ticker/1/?convert=BRL')
    const btcData = response.data
    // console.log('btcData ::; ', btcData);
    return btcData
  } catch (error) {
    console.log('error.btc.info.getBtcInfoAsync ::; ', error);
  }
}
const getLtcInfoAsync = async ( ) => {
  try {
    const response = await axios.get(baseURL + '/v2/ticker/2/?convert=BRL')
    const ltcData = response.data
    // console.log('ltcData ::; ', ltcData);
    return ltcData
  } catch (error) {
    console.log('error.btc.info.getBtcInfoAsync ::; ', error);
  }
}

// LTC to BTC
const LtcToBtc = async (req, res) => {
  try {
    const [btc, ltc] = await axios.all([getBtcInfoAsync(), getLtcInfoAsync()])

    const btcPrice = btc.data.quotes.BRL.price
    const ltcPrice = ltc.data.quotes.BRL.price

    //  LTC
    // {
    //   "Address": "mnf6tupUXzW81Z9BUnXrfSbRjupcBstqWH",
    //   "Account": "xchangesison"
    // }
    ////////////////////////////////////////////////////////
    ////////////////////// money = BRL, coin = cryptocurrency

    // Amount in coin to buy, = example; buy BTC (0.1 satoshi)
    const btcAmountCoinBuy = 0.00001 // 0.000009
    console.log('btcAmountCoinBuy ::; ', btcAmountCoinBuy)

    // Amount in money valued amount coin to buy, = example; (satoshi btc) 0.1 * (btc price) R$30,000.00 = (price to pay) R$3,000.00
    const btcAmountMoneyBuyToLtc = btcAmountCoinBuy * btcPrice
    console.log('btcAmountMoneyBuyToLtc ::; ', btcAmountMoneyBuyToLtc)

    // Amount in coin to change/pay, = example; (price to pay) R$3,000.00 / (ltc price) R$240.00 = (ltc to pay) 12
    const btcAmountCoinBuyToLtc = btcAmountMoneyBuyToLtc / ltcPrice
    console.log('btcAmountCoinBuyToLtc ::; ', btcAmountCoinBuyToLtc)

    // Returns the account associated with the given address.
      const address = req.body.address || 'mnf6tupUXzW81Z9BUnXrfSbRjupcBstqWH';

      ltcClient.getAccount(address, (err, accountFromAddress) => {
        if (err) console.error(err)
        console.log('Account ::; ', accountFromAddress)
        // res.json({'Account ': accountFromAddress, 'Address ': address})

        const btcWalletReceiveCoin = {
          address: 'receiveCoin',
          balance: 0
        }

        const minConf = 0
        ltcClient.getBalance(accountFromAddress, minConf, async (err, balanceFromAccount) => {
          if (err) console.error(err)
          console.log('balanceFromAccount ::; ', balanceFromAccount)
          // res.json({'Account: ': account, 'Balance': balanceFromAccount})
          if (balanceFromAccount >= btcAmountCoinBuyToLtc) {

            const btcAmountCoinBuyToLtcSatoshi = +btcAmountCoinBuyToLtc.toFixed(8)
            console.log('true');
            console.log('balanceFromAccount ::; ', balanceFromAccount)
            console.log('btcAmountCoinBuyToLtc ::; ', btcAmountCoinBuyToLtc)
            // console.log('btcAmountCoinBuyToLtc.toFixed ::; ', +btcAmountCoinBuyToLtc.toFixed(8))

            // LTC
            // {
            //   "Address": "mu4KZwryMExQfb9612bDw9boee1mypcDiP",
            //   "Account": "ExchangeAccount"
            // }
            // BTC
            // {
            //   "account": "Souza",
            //   "address": "n4MddQoqE64rrUSCsKz2Qf4UHc15r1TNLQ"
            // }
            // {
            //   "account": "ExchangeAccount",
            //   "address": "mricJeCPrTq33EAvw5ghm4QRjhyhYmwrr2"
            // }
            const fromAccount = req.body.fromAccount || accountFromAddress; // User account
            const toAddress = req.body.toAddress || 'mu4KZwryMExQfb9612bDw9boee1mypcDiP'; // Exchange address
            const amount = req.body.amount || btcAmountCoinBuyToLtcSatoshi; // amount coins to buy

            ltcClient.sendFrom(fromAccount, toAddress, amount, (err, transacionId) => {
              if (err) console.error(err)
              // btcWalletReceiveCoin.balance = btcAmountCoinBuy;
              // console.log('btcWalletReceiveCoin :;; ', btcWalletReceiveCoin);
              console.log('TransactionIdLtc ::; ', transacionId)
              // d26ec0de40e0441c72fefd3b3fb96fbcbfd7b158077bbb898b7723d044712cf6
              // res.json({'TransactionId ': transacionId})
            })
            const fromAccountBtc = req.body.fromAccountBtc || 'ExchangeAccount' //Exchange account
            const toBitCoinAddress = req.body.toBitCoinAddress || 'n4MddQoqE64rrUSCsKz2Qf4UHc15r1TNLQ' // User address
            const amountBtc = req.body.amountBtc || btcAmountCoinBuy // coins amount to buy
            const txIdBtc = await btcClient.sendFrom(fromAccountBtc, toBitCoinAddress, amountBtc)
            console.log('txIdBtc ::; ', txIdBtc)
            console.log('fromAccountBtc ::; ', fromAccountBtc)
            console.log('toBitCoinAddress ::; ', toBitCoinAddress)
            console.log('amountBtc ::; ', amountBtc)
          } else {
            console.log('false')
            // console.log('ltcWallet.balance ::; ', ltcWallet.balance);
            // console.log('btcAmountCoinBuyToLtc ::; ', btcAmountCoinBuyToLtc);
          }
        })
      })

    // const ltcAmountCoin = 2;
    // const ltcAmountMoney = ltcAmountCoin * ltcPrice
    // const ltcToBtcAmountCoin = ltcAmountMoney / btcPrice
    // console.log('ltcAmountMoney ::; ', ltcAmountMoney);
    // console.log('ltcToBtcAmountCoin ::; ', ltcToBtcAmountCoin);
    // const ltcWallet = {
    //   address: 'sa1Fx=tRe0o19L1Csison',
    //   balance: 20.1337016
    // }
    // const ltcAmountMoneyFromWallet = ltcWallet.balance * ltcPrice
    // const ltcToBtcAmountCoinFromWallet = ltcAmountMoneyFromWallet / btcPrice
    // console.log('ltcAmountMoneyFromWallet ::; ', ltcAmountMoneyFromWallet)
    // console.log('ltcToBtcAmountCoinFromWallet ::; ', ltcToBtcAmountCoinFromWallet)
    // ////////////////////////////////////////////////////////
    // ////////////////////// money = BRL, coin = cryptocurrency
    // const btcAmountCoinBuy = 0.1 // 0.000009
    // const btcAmountMoneyBuyToLtc = btcAmountCoinBuy * btcPrice
    // const btcAmountCoinBuyToLtc = btcAmountMoneyBuyToLtc / ltcPrice
    // console.log('////////////////////////////////////////////////////////')
    //
    // console.log('btcAmountCoinBuy ::; ', btcAmountCoinBuy)
    // console.log('btcAmountMoneyBuyToLtc ::; ', btcAmountMoneyBuyToLtc)
    // console.log('btcAmountCoinBuyToLtc ::; ', btcAmountCoinBuyToLtc)
    //
    // console.log('////////////////////////////////////////////////////////')
    // const btcWalletReceiveCoin = {
    //   address: 'receiveCoin',
    //   balance: 0
    // }
    // if (ltcWallet.balance >= btcAmountCoinBuyToLtc) {
    //   // console.log('true');
    //   // console.log('ltcWallet.balance ::; ', ltcWallet.balance);
    //   // console.log('btcAmountCoinBuyToLtc ::; ', btcAmountCoinBuyToLtc);
    //   // true
    //   // ltcWallet.balance ::;  20.1337016
    //   // btcAmountCoinBuyToLtc ::;  9.490913985084939
    //   const changeLtcToBtc = ltcWallet.balance - btcAmountCoinBuyToLtc
    //   ltcWallet.balance = changeLtcToBtc
    //   console.log('changeLtcToBtc ::; ', changeLtcToBtc)
    //   console.log('ltcWallet.after.buy ::; ', ltcWallet)
    //
    //   const btcReceived = btcWalletReceiveCoin.balance + btcAmountCoinBuy
    //   btcWalletReceiveCoin.balance = btcReceived
    //   console.log('btcReceived ::; ', btcReceived)
    //   console.log('btcWalletReceived.after.buy ::; ', btcWalletReceiveCoin)
    // } else {
    //   console.log('false')
    //   // console.log('ltcWallet.balance ::; ', ltcWallet.balance);
    //   // console.log('btcAmountCoinBuyToLtc ::; ', btcAmountCoinBuyToLtc);
    // }
    // const btcAmountCoin = 2;
    // const btcAmountMoney = btcAmountCoin * btcPrice
    // const btcToLtcAmountCoin = btcAmountMoney / ltcPrice
    // console.log('btcAmountMoney ::; ', btcAmountMoney);
    // console.log('btcToLtcAmountCoin ::; ', btcToLtcAmountCoin);
    // const btcWallet = {
    //   address: 'asHke1tcHUnm81Tseason',
    //   balance: 50.0573633
    // }
    // const btcAmountMoneyFromWallet = btcWallet.balance * btcPrice
    // const btcToLtcAmountCoinFromWallet = btcAmountMoneyFromWallet / ltcPrice
    // console.log('btcAmountMoneyFromWallet ::; ', btcAmountMoneyFromWallet)
    // console.log('btcToLtcAmountCoinFromWallet ::; ', btcToLtcAmountCoinFromWallet)
    //
    // console.log('btcPrice ::; ', btcPrice)
    // console.log('ltcPrice ::; ', ltcPrice)
    res.json({btc: btc, ltc: ltc})
  } catch (error) {
    console.log('error.BtcLtc ::; ', error)
    res.json(error)
  }
}

const BtcToLtc = async (req, res) => {
  try {
    const [btc, ltc] = await axios.all([getBtcInfoAsync(), getLtcInfoAsync()])

    const btcPrice = btc.data.quotes.BRL.price
    const ltcPrice = ltc.data.quotes.BRL.price

    ////////////////////////////////////////////////////////
    ////////////////////// money = BRL, coin = cryptocurrency

    // Amount in coin to buy, = example; buy BTC (0.1 satoshi)
    const ltcAmountCoinBuy = 0.001 // 0.000009
    console.log('ltcAmountCoinBuy ::; ', ltcAmountCoinBuy)

    // Amount in money valued amount coin to buy, = example; (satoshi btc) 0.1 * (btc price) R$30,000.00 = (price to pay) R$3,000.00
    const ltcAmountMoneyBuyToBtc = ltcAmountCoinBuy * ltcPrice
    console.log('ltcAmountMoneyBuyToBtc ::; ', ltcAmountMoneyBuyToBtc)

    // Amount in coin to change/pay, = example; (price to pay) R$3,000.00 / (ltc price) R$240.00 = (ltc to pay) 12
    const ltcAmountCoinBuyToBtc = ltcAmountMoneyBuyToBtc / btcPrice
    console.log('ltcAmountCoinBuyToBtc ::; ', ltcAmountCoinBuyToBtc)

    // Returns the account associated with the given address.
      const address = req.body.address || 'mricJeCPrTq33EAvw5ghm4QRjhyhYmwrr2'; // User btc address
      const accountFromAddress = await btcClient.getAccount(address)
      console.log('accountFromAddress.btc ::; ', accountFromAddress)

      const minConfBtc = 0
      const balanceFromAccount = await btcClient.getBalance(accountFromAddress, minConfBtc)
      console.log('balanceFromAccount.btc ::; ', balanceFromAccount)
      if (balanceFromAccount >= ltcAmountMoneyBuyToBtc) {

        const ltcAmountMoneyBuyToBtcSatoshi = +ltcAmountMoneyBuyToBtc.toFixed(8)
        console.log('true');
        // console.log('balanceFromAccount ::; ', balanceFromAccount)
        console.log('ltcAmountMoneyBuyToBtc ::; ', ltcAmountMoneyBuyToBtc)
        // console.log('btcAmountCoinBuyToLtc.toFixed ::; ', +btcAmountCoinBuyToLtc.toFixed(8))

        // LTC
        // {
        //   "Address": "mnf6tupUXzW81Z9BUnXrfSbRjupcBstqWH",
        //   "Account": "xchangesison"
        // }
        // {
        //   "Address": "mu4KZwryMExQfb9612bDw9boee1mypcDiP",
        //   "Account": "ExchangeAccount"
        // }
        // BTC
        // {
        //   "account": "Souza",
        //   "address": "n4MddQoqE64rrUSCsKz2Qf4UHc15r1TNLQ"
        // }
        // {
        //   "account": "ExchangeAccount",
        //   "address": "mricJeCPrTq33EAvw5ghm4QRjhyhYmwrr2"
        // }
        const fromAccountBtc = req.body.fromAccountBtc || accountFromAddress // User account
        const toBitCoinAddress = req.body.toBitCoinAddress || 'n4MddQoqE64rrUSCsKz2Qf4UHc15r1TNLQ' // Exchange address
        const amountBtc = req.body.amountBtc || ltcAmountMoneyBuyToBtcSatoshi // coins amount to buy
        const txIdBtc = await btcClient.sendFrom(fromAccountBtc, toBitCoinAddress, amountBtc)
        console.log('txIdBtc ::; ', txIdBtc)
        console.log('fromAccountBtc ::; ', fromAccountBtc)
        console.log('toBitCoinAddress ::; ', toBitCoinAddress)
        console.log('amountBtc ::; ', amountBtc)

        const fromAccount = req.body.fromAccount || 'ExchangeAccount' // Exchange account;
        const toAddress = req.body.toAddress || 'mnf6tupUXzW81Z9BUnXrfSbRjupcBstqWH' // User adrress
        const amount = req.body.amount || ltcAmountCoinBuy // coins amount to sell
        ltcClient.sendFrom(fromAccount, toAddress, amount, (err, transacionId) => {
          if (err) console.error(err)
          // btcWalletReceiveCoin.balance = btcAmountCoinBuy;
          // console.log('btcWalletReceiveCoin :;; ', btcWalletReceiveCoin);
          console.log('TransactionIdLtc ::; ', transacionId)
          // d26ec0de40e0441c72fefd3b3fb96fbcbfd7b158077bbb898b7723d044712cf6
          // res.json({'TransactionId ': transacionId})
        })
      } else {
        console.log('false')
        // console.log('ltcWallet.balance ::; ', ltcWallet.balance);
        // console.log('btcAmountCoinBuyToLtc ::; ', btcAmountCoinBuyToLtc);
      }
      res.json({btc: btc, ltc: ltc})
  } catch (error) {
    console.log('error.BtcLtc ::; ', error)
    res.json(error)
  }
}

router.get('/btc', getBtcInfo)
router.get('/ltc', getLtcInfo)
router.get('/ltcbtc', LtcToBtc)
router.get('/btcltc', BtcToLtc)

module.exports = router
